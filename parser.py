#!/usr/bin/env python3
import re
import json
in_file_name = "workingdir/text.txt"


data_key_identifier = [
["Empfohlene bzw. erwartete","Vorkenntnisse"],
["Modulprüfung"],
["Inhalte"],
["- Präsenzstunden"],
["- Selbststudium"],
["Voraussetzung für die Zulassung", "zum Modul"],
["Zusammensetzung des Moduls /"],
["Arbeitsaufwand (work load) in:"],
["Art des Moduls (Pflicht-, Wahlpflicht-","oder Wahlmodul)"],
["Modul-Verantwortliche/r"],
["Dauer des Moduls"],
["Häufigkeit des Angebots","(Modulturnus)"],
["Leistungspunkte (ECTS credits)"],
["Modulcode"],
["Modultitel (deutsch)"],
["Modultitel (englisch)"]
]


in_file = open(in_file_name,"r")

lines = in_file.readlines()

# cut TOC and abbreviations at the end

explains_line_number = -1
abbreviations_line_number = -1

for i in range(0,len(lines)):
    if lines[i].find("Erläuterung zum Modulkatalog") != -1:
        explains_line_number = i
    if lines[i].find("Abkürzungen für Veranstaltungen") != -1: #find only first
        abbreviations_line_number = i
        break
if explains_line_number == -1 or abbreviations_line_number == -1:
     raise Exception("couldnt find explain or abbreviations part")

for i in range(explains_line_number,len(lines)):
    match = re.search(r'Seite \d{1,4} von \d{1,4}',lines[i])
    if match != None:
        explains_line_number = i
        break

lines = lines[explains_line_number:abbreviations_line_number]

lines = [elem for elem in lines if re.search(r'Seite \d{1,4} von \d{1,4}',elem) == None]
lines = [elem for elem in lines if len(elem) > 1]
#lines = [elem.strip() for elem in lines]

module_lines = []

modules = []

for i in range(0,len(lines)):
    if re.search(r'^ {3,5}Modul ',lines[i]) != None:
        module_lines.append(i)

#print("found {0} modules".format(len(module_lines)))
module_lines.append(len(lines))
for line_number_number in range(0,len(module_lines)-1):
    colm_start_data = -1
    module = {}
    for i in range(module_lines[line_number_number],module_lines[line_number_number+1]):
        if lines[i].find("Modulcode") != -1:
            colm_start_data = re.search(r'^ {2,4}(\S+[ \n])+\s+',lines[i]).end()

        if colm_start_data != -1:
            for j in data_key_identifier:
                if lines[i].find(j[0]) != -1 :
                    module[j[0]] = lines[i][colm_start_data:]
                    if len(j)>1:
                        module[j[0]] += " " + lines[i+1][colm_start_data:]

    modules.append(module)
    #
    #    match = re.search(r'^ {2,4}(\S+[ \n])+\s+',lines[i])
    #    print(match)
        #if match != None:
            #print(match.group().strip())

print(json.dumps(modules))

#for i in module_lines[:-1]:
#    print(lines[i].strip())
